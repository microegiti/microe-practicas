library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
    generic (
        WIDTH : integer := 32
    );
    port (
        rst :  in std_logic;
        clk :  in std_logic;
        a   :  in std_logic;
        b   :  in std_logic;
        c   :  in std_logic;
        d   :  in std_logic;
        sum : out unsigned(WIDTH-1 downto 0)
    );
end top;

architecture Behavioral of top is

    signal calc, p_calc: unsigned(WIDTH-1 downto 0);
    signal calc1, p_calc1: unsigned(WIDTH-1 downto 0);
    signal calc2, p_calc2: unsigned(WIDTH-1 downto 0);
    signal lfsr_a, p_lfsr_a: std_logic_vector(31 downto 0);
    signal lfsr_b, p_lfsr_b: std_logic_vector(31 downto 0);
    signal lfsr_c, p_lfsr_c: std_logic_vector(31 downto 0);
    signal lfsr_d, p_lfsr_d: std_logic_vector(31 downto 0);

begin

    -- Our operation is a sum of 4 pseudo random numbers of up to 32 bits each.
    -- We use pseudo_random numbers as input so we do not take all the I/Os of
    -- small FPGA devices
    p_calc <= calc1 + calc2;
    p_calc1 <= unsigned(lfsr_a(WIDTH-1 downto 0)) +
               unsigned(lfsr_b(WIDTH-1 downto 0));
    p_calc2 <= unsigned(lfsr_c(WIDTH-1 downto 0)) +
               unsigned(lfsr_d(WIDTH-1 downto 0));

    -- The combinational process just implements the logic of the LFSRs
    comb: process(a, b, c, d, lfsr_a, lfsr_b, lfsr_c, lfsr_d)
    begin
        p_lfsr_a <= lfsr_a;
        p_lfsr_b <= lfsr_b;
        p_lfsr_c <= lfsr_c;
        p_lfsr_d <= lfsr_d;
        if a = '1' then
            p_lfsr_a <= lfsr_a(30 downto 0) & (lfsr_a(31) XOR lfsr_a(21) XOR lfsr_a(1) XOR lfsr_a(0));
        end if;
        if b = '1' then
            p_lfsr_b <= lfsr_b(30 downto 0) & (lfsr_b(31) XOR lfsr_b(21) XOR lfsr_b(1) XOR lfsr_b(0));
        end if;
        if c = '1' then
            p_lfsr_c <= lfsr_c(30 downto 0) & (lfsr_c(31) XOR lfsr_c(21) XOR lfsr_c(1) XOR lfsr_c(0));
        end if;
        if d = '1' then
            p_lfsr_d <= lfsr_d(30 downto 0) & (lfsr_d(31) XOR lfsr_d(21) XOR lfsr_d(1) XOR lfsr_d(0));
        end if;
    end process;

    -- A synchronous process implements all required flip-flops
    sync: process(rst, clk)
    begin
        if rst = '1' then
            calc <= (others=>'0');
            calc1 <= (others=>'0');
            calc2 <= (others=>'0');
            lfsr_a <= (others=>'0');
            lfsr_b <= (others=>'0');
            lfsr_c <= (others=>'0');
            lfsr_d <= (others=>'0');
        elsif rising_edge(clk) then
            calc <= p_calc;
            calc1 <= p_calc1;
            calc2 <= p_calc2;
            lfsr_a <= p_lfsr_a;
            lfsr_b <= p_lfsr_b;
            lfsr_c <= p_lfsr_c;
            lfsr_d <= p_lfsr_d;
        end if;
    end process;

    -- Output the sum we have just calculated
    sum <= calc;

end Behavioral;
