-- Multiplexor de dos entradas
library ieee;
use ieee.std_logic_1164.all;

entity mux2a1 is
  port (
    e1 : in  std_logic;
    e2 : in  std_logic;
    sel: in  std_logic;
    o1 : out std_logic
  );
end mux2a1;

architecture mux2a1_arch of mux2a1 is

begin

  process(e1,e2, sel)
  begin
    if (sel='0') then
      o1 <= e1;
    else
      o1 <= e2;
    end if;
  end process;

end mux2a1_arch;
