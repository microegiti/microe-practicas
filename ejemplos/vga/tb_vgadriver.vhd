library ieee;
use ieee.std_logic_1164.all;

entity tb_vgadriver is
end tb_vgadriver;

architecture tb_vgadriver_arch of tb_vgadriver is

  -- Signals for the UUT
  signal clk, rst, hs, vs : std_ulogic;
  signal r, g, b          : std_ulogic;
  signal button_left, button_center, button_right: std_ulogic;

  -- Simulation control
  -- clk freq is 25 MHz
  constant NCYCLES    : integer := 2*640*480+1000;
  constant clk_period : time    := 40 ns;
  signal endsim       : boolean := false;

begin

  -- Instance of the UUT
  vgadriver_inst : entity work.vgadriver
    port map (
      clk => clk,
      rst => rst,
      button_left => button_left,
      button_center => button_center,
      button_right => button_right,
      hs  => hs,
      vs  => vs,
      r   => r,
      g   => g,
      b   => b
      );

  -- clk generation
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
    if endsim = true then
      wait;
    end if;
  end process;

  -- Rest of stimuli
  stim_process : process
  begin
    rst    <= '1';
    wait for 4*clk_period;
    rst    <= '0';
    wait for NCYCLES*clk_period;
    endsim <= true;
    wait;
  end process;


end tb_vgadriver_arch;
