# Microelectrónica

Intrucciones del software que usaremos para realizar el trabajo de VHDL

## Contenidos

[TOC]


# Configuración de la máquina virtual e instalación de las herramientas

Para disponer de la máquina virtual con el software que utilizaremos para la asignatura, debemos seguir los siguientes pasos:

Descargar la máquina virtual del siguiente enlace y seguir las instrucciones: http://trajano.us.es/~fjfj/mv/maq_virtual.html . No es necesario seguir las instrucciones de la sección “Fase 4. Instalación de software adicional”.

Esta máquina virtual es una copia de la distribución de linux existente en el Centro de Cálculo. Si ya la tenéis en vuestro ordenador, podéis omitir este primer paso.

Se debe utilizar el usuario salas, cuyo password es también salas. Este usuario tiene permisos para hacer sudo.

Instalar las dependencias del software, ejecutando el siguiente comando en un terminal:

    sudo apt update
    sudo apt install gnat zlib1g-dev lcov libcanberra-gtk-module gtkwave libboost-all-dev libftdi1

Descargar y descomprimir el software para la asignatura:

    cd ~
    wget https://heimdall.us.es/sw/fosshdl-20230904.tar.gz
    tar xzvf fosshdl-20230904.tar.gz

Se puede comprobar que la descarga ha sido correcta utilizando el siguiente comando:

    md5sum fosshdl-20230904.tar.gz

La salida de dicho comando debe incluir este código (si el código es distinto, significa que ha habido un error en la descarga):

    492bf3213cef5a7c4ada191b1e437530  fosshdl-20230904.tar.gz

Permitir el acceso a los dispositivos usb al usuario salas:

    sudo sh -c 'echo "SUBSYSTEM==\"usb\", MODE=\"0660\", GROUP=\"plugdev\"" > /etc/udev/rules.d/00-usb-permissions.rules'

En la(s) consola(s) en la(s) que vayamos a utilizar el software, cargar las variables de entorno:

    source ~/fosshdl/env.rc

# Editor de texto

Se puede utilizar cualquier editor de texto para editar los ficheros con los que trabajaremos en las prácticas (principalmente ficheros VHDL y Makefiles).


## TerosHDL

Para la asignatura os puede ser interesante utilizar la extensión
[TerosHDL](https://github.com/TerosTechnology/terosHDL) del editor Visual
Studio Code. Esta extensión, cuyos desarrolladores principales son antiguos
alumnos de la Escuela Técnica Superior de Ingeniería de la Universidad de
Sevilla, es software libre y ofrece muchas funcionalidades integradas para
facilitar el desarrollo con VHDL y Verilog.

### Instalación de TerosHDL

- El primer paso es instalar Visual Studio Code: https://code.visualstudio.com/Download
- El segundo paso es instalar las librerías que necesita TerosHDL:

        sudo apt install python3-pip
        pip3 install teroshdl

- El tercer paso es abrir Visual Studio Code, ir a 'Extensions', buscar
  TerosHDL y hacer click en 'Install'
- Para hacer una primera toma de contacto con el editor, pueden seguir esta [práctica de introducción a TerosHDL](https://gitlab.com/edcmit/edc-practicas/-/tree/main/practica1) que he preparado para otra asignatura, en la que están desglosados los pasos para crear un proyecto y lanzar simulaciones.
- También es muy recomendable leer la [documentación completa de
  TerosHDL](https://terostechnology.github.io/terosHDLdoc/)

## Otros editores

### gedit

Quien esté cómodo editando ficheros de texto plano en un editor gráfico
sencillo, puede usar ``gedit``:

        sudo apt install gedit

### vim o emacs

Quien ya sepa manejar editores más complejos como ``vim`` o ``emacs`` puede
utilizarlos también. En particular, existen distribuciones de emacs como [Doom
emacs](https://github.com/hlissner/doom-emacs) o
[Spacemacs](https://www.spacemacs.org/) que ya incluyen paquetes para trabajar
con VHDL.

# Manejo del simulador GHDL
Para poder simular un diseño con ghdl, es necesario realizar los siguientes pasos:

Primero se deben analizar todos los ficheros VHDL, empezando por los packages y los ficheros de menor nivel en la jerarquía. Por cada fichero VHDL, se debe ejecutar el comando:

    ghdl -a fichero.vhd
    
Dicho comando generará un fichero objeto, con el mismo nombre que el fichero .vhd pero de extensión .o

Una vez analizados todos los ficheros, se elabora el top-level de nuestro testbench:

    ghdl -e tb_entity

Donde tb_entity debe ser el nombre del entity de nuestro testbench. Este paso generará un fichero que, al ejecutarse, realizará la simulación.

Ejecutamos el fichero generado, indicando al simulador que genere un fichero de forma de onda en formato .ghw:

    ./tb_entity --wave=tb_entity.ghw

Si no tenemos ningún mecanismo para conseguir que la simulación pare automáticamente (por ejemplo, dejando de generar eventos de clk a partir de cierto número de ciclos), debemos utilizar la opción ``--stop-time=<TIME>``, por ejemplo:

    ./tb_entity --wave=tb_entity.ghw --stop-time=500ns

Si durante cualquiera de los pasos se detecta algún error en el código del usuario, la herramienta indicará el fichero y la línea donde considera que tuvo origen el error.

# Visualización de formas de onda

Para visualizar las formas de onda, utilizaremos el visor de formas de onda gtkwave. Gtkwave está disponible en los repositorios de las distribuciones linux más extendidas como pueden ser debian, ubuntu, centos o redhat.

Para lanzar el visor de ondas:

    gtkwave fichero.ghw

El botón ‘reload’ de la herramienta permite volver a cargar el fichero .ghw sin tener que cerrar y volver a abrir el programa (ni volver a configurar las formas de onda). De esta forma podemos lanzar de nuevo la simulación, en otro terminal, y cuando haya terminado simplemente pulsar el botón ‘reload’ de gtkwave.


# Síntesis con ghdl y yosys

Ya que la versión libre de yosys únicamente soporta el lenguaje verilog, utilizaremos el plugin ghdlsynth para yosys

Primero tenemos que analizar con ghdl todos los ficheros fuente que vayamos a sintetizar, en orden de menor a mayor nivel en la jerarquía:

    ghdl -a fichero1.vhd
    ghdl -a fichero2.vhd
    ghdl -a top.vhd

Suponiendo que la entidad descrita en top.vhd se llame top, una vez que hemos analizado todos los ficheros fuente, comprobamos si existe algún error con el siguiente comando:

    ghdl --synth top

Si a la salida de este comando obtenemos una descripción VHDL pero a bajo nivel, es que la síntesis ha sido correcta. Sin embargo, si obtenemos un mensaje de error de este tipo:

    top.vhd:18:5:error: latch infered for net "salida1"

Es que tenemos algo que corregir en nuestro código, por ejemplo en este caso sería la presencia de un latch. Ghdl nos avisa si tenemos latches, pero no nos avisa si tenemos listas de sensibilidad incompletas, por lo que tendremos que tener especial cuidado con ellas, ya que nos pueden afectar a nuestras simulaciones, aunque no nos afecten a la síntesis.

Una vez que no encontremos errores con ghdl --synth, realizamos la síntesis del top_level:

    yosys -m ghdl -p 'ghdl top.vhd -e top; synth_ice40 -json top.json'

Este comando viene a significar: utilizamos yosys con el módulo de ghdl y elaboramos el fichero top.vhd cuyo entity se llama top. Sintetizamos para FPGAs de la familia ICE40 y guardaremos la salida en el fichero top.json, que contendrá nuestro diseño sintetizado.


# Place and route con nextpnr

Para rutar el diseño sintetizado, basta con ejecutar el siguiente comando:

    nextpnr-ice40 --up5k --package sg48 --pcf top.pcf --json top.json --asc top.asc

up5k es el modelo dentro de la familia de FPGAs ice40 que vamos a utilizar. En particular, la FPGA que monta la tarjeta icebreaker tiene un encapsulado tipo sg48. Dicha familia de FPGA ha sido desarrollada por el fabricante Lattice Semiconductor. El fichero top.pcf es un fichero de asignación de pines, en el que indicamos al software a qué pines de la FPGA deben ir conectados los puertos del top-level de nuestro diseño. El fichero json es el diseño sintetizado generado en el paso anterior. El fichero .asc contendrá el diseño rutado.

Siempre que utilicemos diseños que tengan reloj, se debe añadir también el argumento --freq seguido de la frecuencia del reloj en MHz (12 MHz en la tarjeta icebreaker).

La sintaxis de las líneas del fichero .pcf (pin constraints file) es la siguiente:

    set_io nombre_puerto_top numero_pin_fpga

Por ejemplo:

    set_io boton 10
    set_io led 26

A modo de guía de localización de los pines, se puede encontrar un fichero .pcf que incluye todos los pines de la FPGA para la tarjeta que vamos a utilizar en el siguiente enlace: https://github.com/icebreaker-fpga/icebreaker-verilog-examples/blob/main/icebreaker/icebreaker.pcf 

# Generación del bitstream con icestorm

El último paso necesario es convertir el diseño rutado al formato de bitstream la FPGA:

    icepack top.asc top.bin

Este comando convierte el fichero con el diseño rutado top.asc en un bitstream para la FPGA ice40up5k (top.bin), con el que se puede directamente configurar la FPGA.

# Configuración de la FPGA con iceprog

Para configurar la FPGA, utilizaremos la herramienta iceprog:

    iceprog top.bin

Aunque estamos ante una FPGA de SRAM, si desconectamos y volvemos a conectar la FPGA, veremos que la configuración se almacena de manera no volátil, ya que lo que hace esta herramienta es configurar la memoria flash de la tarjeta, de donde la FPGA leerá el bitstream.

# Automatización de los pasos

Al poderse realizar todos a través de la línea de comandos, se pueden automatizar estos pasos para un diseño concreto (por ejemplo el trabajo de curso) utilizando un Makefile o un script de consola. Se pueden ver ejemplos de Makefiles en la carpeta [ejemplos](ejemplos) (en particular, el Makefile del ejemplo del driver VGA es el más completo y les servirá de base para el trabajo)

# Más información sobre la tarjeta icebreaker

Para más información sobre la tarjeta, así como consultar su documentación, podemos ir al siguiente enlace: https://icebreaker-fpga.org/ 


# Agradecimientos / Acknowledgement

Thank you to all developers and maintainers of the open source tools for
FPGA design and verification: Claire Wolf, Tristan Gingold, Myrtle Shah, Unai
Martínez-Corral, Matt Venn and many, many others...

Thank you also to Piotr Esden-Tempski (and the rest of [contributors](https://github.com/icebreaker-fpga/icebreaker/graphs/contributors)) for
creating the icebreaker FPGA board.

Finally, thanks to [Francisco José Fernández
Jiménez](http://trajano.us.es/~fjfj) for providing and maintaining, year after
year, a Virtual Machine that mimics the operating system in ETSI's computer
rooms. Your work has saved us a lot of time all these years.
